/*
Advent of code, day 9.
*/
#include <fstream>
#include <iostream>
#include <iterator>
#include <sstream>

#include "absl/status/status.h"
#include "absl/status/statusor.h"

namespace {

absl::StatusOr<std::vector<int>> ParseLine(const std::string& line) {
  std::istringstream instream(line);
  std::istream_iterator<int> begin(instream), end;
  return std::vector<int>(begin, end);
}

// Compute the differences, then sum up to the next.
int PredictNext(std::vector<int> nums) {
  int iters;
  for (iters = 1; iters < nums.size(); ++iters) {
    // Do the diff, except for the last value.
    bool all_zero = true;
    for (int i = 0; i < nums.size() - iters; ++i) {
      nums[i] = nums[i + 1] - nums[i];
      all_zero = all_zero && (nums[i] == 0);
    }
    if (all_zero) {
      break;
    }
  }
  // All items from [0, nums.size()-iters) are 0.
  // Sum the rest.
  int sum = 0;
  for (int i = nums.size() - iters; i < nums.size(); ++i) {
    std::clog << nums[i] << " ";
    sum += nums[i];
  }
  std::clog << "-> ";
  return sum;
}

// Predict the previous by doing the same thing, but scanning from the end,
// then differencing up the remainder.
// TODO: merge with the previous method.
int PredictPrev(std::vector<int> nums) {
  int iters;
  for (iters = 1; iters < nums.size(); ++iters) {
    bool all_zero = true;
    for (int i = nums.size() - 1; i >= iters; --i) {
      nums[i] = nums[i] - nums[i - 1];
      all_zero = all_zero && (nums[i] == 0);
    }
    if (all_zero) {
      break;
    }
  }
  // All items from [iters, nums.size()) are 0.
  // Cumulatively subtract the rest.
  int accum = 0;
  for (int i = iters - 1; i >= 0; --i) {
    std::clog << nums[i] << " ";
    accum = nums[i] - accum;
  }
  std::clog << "-> ";
  return accum;
}

}  // namespace

int main(int argc, char* argv[]) {
  if (argc < 2) {
    std::cerr << "requires path" << std::endl;
    return 1;
  }
  std::ifstream input;
  input.open(argv[1]);

  int next_sum = 0, prev_sum = 0;
  std::string line;
  while (std::getline(input, line)) {
    auto nums = ParseLine(line);
    if (!nums.ok()) {
      std::cerr << nums.status() << std::endl;
      return 1;
    }
    int next = PredictNext(*nums);
    std::clog << next << std::endl;
    int prev = PredictPrev(*nums);
    std::clog << prev << std::endl;
    next_sum += next;
    prev_sum += prev;
  }
  std::cout << next_sum << std::endl;
  std::cout << prev_sum << std::endl;
  return 0;
}
