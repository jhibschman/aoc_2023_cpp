/*
Advent of code, day 3: https://adventofcode.com/2023/day/3

Given a grid of data, like:

  467..114..
  ...*......
  ..35..633.
  ......#...
  617*......
  .....+.58.
  ..592.....
  ......755.
  ...$.*....
  .664.598..

Part 1: Find all numbers adjacent to a symbol and add them.

Part 2: Find all "gears", which are '*' characters with exactly two
adjacent numbers, and sum their "gear ratio", which is the product of
the two numbers.

*/

#include <cctype>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "absl/strings/numbers.h"
#include "absl/strings/string_view.h"

using Grid = std::vector<std::string>;

namespace {

constexpr char kDigits[] = "0123456789";

Grid ReadGrid(std::istream& input) {
  Grid grid;
  std::string line;
  while (std::getline(input, line)) {
    grid.push_back(line);
  }
  return grid;
}

bool IsSymbol(char ch) {
  if (ch >= '0' && ch <= '9') {
    return false;
  }
  return ch != '.';
}

// Half-open interval [start_col, end_col).
bool HasAdjacentSymbol(const Grid& grid, int row, int start_col, int end_col) {
  // Top line.
  int prev_row_size = row > 0 ? grid[row - 1].size() : 0;
  int next_row_size = row < grid.size() - 1 ? grid[row + 1].size() : 0;
  // Check prev and next row.
  for (int c = start_col - 1; c < end_col + 1; ++c) {
    if (c >= 0 && c < prev_row_size && IsSymbol(grid[row - 1][c])) {
      return true;
    }
    if (c >= 0 && c < next_row_size && IsSymbol(grid[row + 1][c])) {
      return true;
    }
  }
  // Check edges of current row.
  if (start_col - 1 >= 0 && IsSymbol(grid[row][start_col - 1])) {
    return true;
  }
  if (end_col < grid[row].size() && IsSymbol(grid[row][end_col])) {
    return true;
  }
  return false;
}

// A parsed number and the half-open [start, end) span of the string
// it came from.
struct NumberSpan {
  int value;
  int start, end;
};

// Given that (r, c) is a digit, find the full number span containing it.
NumberSpan NumberSpanContaining(const Grid& grid, int r, int c) {
  // pre:
  //  r is legal row    (0 <= r < grid.size()),
  //  c is legal column (0 <= c < grid[r].size()),
  //  isdigit(grid[r][c]).
  absl::string_view row(grid[r]);
  int start_m1 = row.find_last_not_of(kDigits, c);
  int start = start_m1 == std::string::npos ? 0 : start_m1 + 1;
  int end = row.find_first_not_of(kDigits, c);
  end = end == std::string::npos ? row.size() : end;
  int num = 0;
  if (!absl::SimpleAtoi(row.substr(start, end - start), &num)) {
    // TODO: Should I return a StatusOr, or an optional?
    return {.value = -1, .start = -1, .end = -1};
  }
  return {.value = num, .start = start, .end = end};
}

// Sum the numbers that have a symbol adjacent.
int SumNumbersWithAdjacentSymbols(const Grid& grid, bool debug = false) {
  // Scan for numbers.
  int sum = 0;
  for (int row_i = 0; row_i < grid.size(); ++row_i) {
    if (debug) {
      std::clog << "row_i: " << row_i << std::endl;
    }
    const std::string& row = grid[row_i];
    int pos = -1;
    while (true) {
      pos = row.find_first_of(kDigits, pos + 1);
      if (pos == std::string::npos) {
        break;
      }
      // Found a digit, now scan for the entire number.
      auto span = NumberSpanContaining(grid, row_i, pos);
      if (span.start == -1) {
        std::cerr << "failed to parse" << std::endl;
        return 1;
      }
      if (debug) {
        std::clog << "val: " << span.value << std::endl;
      }
      // Check if it has an adjacent symbol. This could be more clever,
      // but it's not that big a problem, so simple is better.
      if (HasAdjacentSymbol(grid, row_i, span.start, span.end)) {
        if (debug) {
          std::clog << "has symbol" << std::endl;
        }
        sum += span.value;
      } else if (debug) {
        std::clog << "no symbol" << std::endl;
      }
      // Skip to the end.
      pos = span.end;
    }
  }
  return sum;
}

// Find the numbers adjacent to a given point.
// This doesn't *require* that grid[r][c] not be a digit, but that is
// the usual case. If grid[r][c] is a digit, it will be included.
std::vector<int> AdjacentNumbers(const Grid& grid, int r, int c) {
  std::vector<int> numbers;
  for (int dr = -1; dr <= 1; ++dr) {  // delta row
    bool prev_was_digit =
        false;  // So if we have 123, we don't report it 3 times.
    for (int dc = -1; dc <= 1; ++dc) {  // delta column
      // It doesn't matter if we scan over dr=0, dc=0.
      int rp = r + dr;  // row prime
      int cp = c + dc;  // col prime
      if (rp < 0 || rp >= grid.size() || cp < 0 || cp >= grid[rp].size()) {
        prev_was_digit = false;
        continue;
      }
      if (!std::isdigit(grid[rp][cp])) {
        prev_was_digit = false;
        continue;
      }
      if (prev_was_digit) {  // Don't double-count numbers.
        continue;
      }
      numbers.push_back(NumberSpanContaining(grid, rp, cp).value);
      prev_was_digit = true;
    }
  }
  return numbers;
}

// Sum the "gear ratios".
// - A "gear" is a "*" that has exactly two numbers adjacent to it.
// - The "gear ratio" of that gear is the product of the two numbers.
int SumGearRatios(const Grid& grid, bool debug = false) {
  int sum = 0;
  for (int r = 0; r < grid.size(); ++r) {
    const std::string& row = grid[r];
    for (int c = 0; c < row.size(); ++c) {
      if (row[c] != '*') {
        continue;
      }
      auto nums = AdjacentNumbers(grid, r, c);
      if (nums.size() == 2) {
        sum += nums[0] * nums[1];
        if (debug) {
          std::clog << "r=" << r + 1 << ", c=" << c + 1 << ", n0=" << nums[0]
                    << ", n1=" << nums[1] << std::endl;
        }
      }
    }
  }
  return sum;
}

}  // namespace

int main(int argc, char* argv[]) {
  if (argc < 2) {
    std::cerr << "path needed" << std::endl;
    return 1;
  }
  const std::string path = argv[1];
  std::ifstream file;
  file.open(path);

  // Read the data.
  Grid grid = ReadGrid(file);
  int sum = SumNumbersWithAdjacentSymbols(grid);
  int gear_sum = SumGearRatios(grid);
  std::cout << sum << std::endl;
  std::cout << gear_sum << std::endl;
  return 0;
}