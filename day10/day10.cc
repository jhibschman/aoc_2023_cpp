/*
Advent of code, day 10: https://adventofcode.com/2023/day/10

Incomplete solution. I should be figuring out what tile the "start"
tile is, then classifying it into a "vertical" or not tile, rather
than just using a constant.
*/

#include <fstream>
#include <iostream>
#include <set>
#include <utility>
#include <vector>

#include "absl/status/status.h"
#include "absl/status/statusor.h"

namespace {
using Grid = std::vector<std::string>;
using Point = std::pair<int, int>;      // row, col
using Direction = std::pair<int, int>;  // delta-row, delta-col

absl::StatusOr<Grid> ParseGrid(std::istream& input) {
  std::string line;
  Grid grid;
  int ncols = 0;
  while (std::getline(input, line)) {
    if (grid.empty()) {
      ncols = line.size();
    } else if (ncols != line.size()) {
      return absl::InvalidArgumentError("col size mismatch");
    }
    grid.push_back(line);
  }
  if (grid.empty()) {
    return absl::InvalidArgumentError("empty grid");
  }
  return grid;
}

absl::StatusOr<Point> FindStart(const Grid& grid) {
  for (int r = 0; r < grid.size(); ++r) {
    const std::string& row = grid[r];
    for (int c = 0; c < row.size(); ++c) {
      if (row[c] == 'S') {
        return Point{r, c};
      }
    }
  }
  return absl::InvalidArgumentError("start not found");
}

bool InBounds(const Grid& g, Point p) {
  bool row_ok = p.first >= 0 && p.first < g.size();
  bool col_ok = p.second >= 0 && p.second < g[0].size();
  return row_ok && col_ok;
}

// Doesn't check bounds.
Point AddDirection(Point p, Direction d) {
  return {p.first + d.first, p.second + d.second};
}

// Is it legal to move between two points.
bool CanMoveToPoint(const Grid& g, Point from, Point to) {
  if (!InBounds(g, from) || !InBounds(g, to)) {
    return false;
  }
  int dr = to.first - from.first;
  int dc = to.second - from.second;
  switch (g[from.first][from.second]) {
    case '.':
      return false;
    case '|':
      return dc == 0 && (dr == 1 || dr == -1);
    case '-':
      return dr == 0 && (dc == 1 || dc == -1);
    case 'L':
      return (dr == -1 && dc == 0) || (dr == 0 && dc == 1);
    case 'J':
      return (dr == -1 && dc == 0) || (dr == 0 && dc == -1);
    case '7':
      return (dr == 1 && dc == 0) || (dr == 0 && dc == -1);
    case 'F':
      return (dr == 1 && dc == 0) || (dr == 0 && dc == 1);
    default:
      return false;
  }
}

// // Is it legal?
// bool CouldComeFrom(const Grid& grid, Point p, Direction d) {
//   Point p2 = AddDirection(p, d);
//   if (!InBounds(grid, p2)) {
//     return false;
//   }
//   return CanMoveToPoint(grid, p2, p);
// }

const std::vector<Direction>& DirsFromChar(char ch) {
  static std::vector<std::vector<Direction>>* dir_list = []() {
    return new std::vector<std::vector<Direction>>{
        {},                  // .
        {{1, 0}, {-1, 0}},   // |
        {{0, 1}, {0, -1}},   // -
        {{-1, 0}, {0, 1}},   // L
        {{-1, 0}, {0, -1}},  // J
        {{1, 0}, {0, -1}},   // 7
        {{1, 0}, {0, 1}},    // F
    };
  }();
  switch (ch) {
    case '|':
      return (*dir_list)[1];
    case '-':
      return (*dir_list)[2];
    case 'L':
      return (*dir_list)[3];
    case 'J':
      return (*dir_list)[4];
    case '7':
      return (*dir_list)[5];
    case 'F':
      return (*dir_list)[6];
    default:
      return (*dir_list)[0];
  }
}

char CharAt(const Grid& g, Point p) { return g[p.first][p.second]; }

Point NextPoint(const Grid& g, const std::vector<Point>& path) {
  Point prev = path[path.size() - 2];
  for (Direction d : DirsFromChar(CharAt(g, path.back()))) {
    Point p2 = AddDirection(path.back(), d);
    if (p2 != prev) {
      return p2;
    }
  }
  return {-1, -1};  // should never happen
}

std::ostream& LogPoint(Point p) {
  std::clog << "(" << p.first << "," << p.second << ")";
  return std::clog;
}

bool ExtendPath(const Grid& g, std::vector<Point>& path) {
  Point p2 = NextPoint(g, path);
  LogPoint(p2) << std::endl;
  if (p2 == path[0] || p2.first == -1) {
    return false;
  }
  path.push_back(p2);
  return true;
}

// Find the loop in the grid.
absl::StatusOr<std::vector<Point>> FindLoop(const Grid& grid, Point start) {
  // Check the four directions.
  std::vector<Direction> directions = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};
  std::vector<Point> possible_nexts;
  for (Direction d : directions) {
    Point p2 = AddDirection(start, d);
    if (CanMoveToPoint(grid, p2, start)) {
      possible_nexts.push_back(p2);
    }
  }
  if (possible_nexts.size() != 2) {
    return absl::InvalidArgumentError("should be exactly two options");
  }
  std::vector<Point> path{start, possible_nexts[0]};
  while (ExtendPath(grid, path))
    ;
  return path;
}

int FindContained(const Grid& g, const std::vector<Point>& loop,
                  bool s_is_vertical) {
  const std::set<Point> loop_set(loop.begin(), loop.end());
  int contained = 0;
  for (int r = 0; r < g.size(); ++r) {
    int verticals_crossed = 0;
    for (int c = 0; c < g[r].size(); ++c) {
      Point p{r, c};
      char ch = CharAt(g, p);
      if (!loop_set.count(p)) {
        // Anything not part of the loop counts as a potential "contained" tile.
        if (verticals_crossed % 2) {
          std::clog << "I: ";
          LogPoint(p);
          std::clog << std::endl;
        }
        contained += verticals_crossed % 2;
        continue;
      }
      switch (ch) {
        case 'S':
          verticals_crossed += s_is_vertical;
          break;
        case '|':
        case 'J':
        case 'L':
          ++verticals_crossed;
          break;
        case '.':  // shouldn't happen
        case '-':
        case '7':  // end horizontal
        case 'F':  // end horizontal
          break;
      }
    }
  }
  return contained;
}

absl::Status DoPart1And2(std::istream& input) {
  auto grid = ParseGrid(input);
  if (!grid.ok()) {
    return grid.status();
  }
  auto start = FindStart(*grid);
  if (!start.ok()) {
    return start.status();
  }
  auto loop = FindLoop(*grid, *start);
  if (!loop.ok()) {
    return loop.status();
  }
  for (Point p : *loop) {
    std::clog << "(" << p.first << "," << p.second << "), ";
  }
  std::clog << std::endl;
  std::cout << loop->size() / 2 << std::endl;
  std::cout << FindContained(*grid, *loop, false) << std::endl;
  return absl::OkStatus();
}

}  // namespace

int main(int argc, char* argv[]) {
  if (argc < 2) {
    std::cerr << "path required" << std::endl;
    return 1;
  }
  std::ifstream input;
  input.open(argv[1]);
  auto status = DoPart1And2(input);
  if (!status.ok()) {
    std::cerr << status << std::endl;
    return 1;
  }
  return 0;
}
