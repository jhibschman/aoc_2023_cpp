#include "day07_lib.h"

#include <algorithm>
#include <array>
#include <iostream>
#include <sstream>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

#include "absl/status/status.h"
#include "absl/status/statusor.h"
#include "absl/strings/numbers.h"

namespace aoc07 {

absl::StatusOr<Hand> ParseHand(const std::string& text) {
  if (text.size() != 5) {
    return absl::InvalidArgumentError("hand must be 5 chars");
  }
  Hand h;
  for (int i = 0; i < 5; ++i) {
    char ch = text[i];
    if (ch >= '2' && ch <= '9') {
      h[i] = ch - '0';
      continue;
    }
    Card c;
    switch (ch) {
      case 'T':
        c = 10;
        break;
      case 'J':
        c = 11;
        break;
      case 'Q':
        c = 12;
        break;
      case 'K':
        c = 13;
        break;
      case 'A':
        c = 14;
        break;
      default:
        return absl::InvalidArgumentError("unrecognized card");
    }
    h[i] = c;
  }
  return h;
}

Hand ConvertJacksToJokers(const Hand& hand) {
  Hand h = hand;
  for (int i = 0; i < 5; ++i) {
    if (h[i] == 11) {
      h[i] = 1;
    }
  }
  return h;
}

std::vector<std::pair<Hand, Bid>> ConvertJacksToJokers(
    const std::vector<std::pair<Hand, Bid>>& hand_bids) {
  std::vector<std::pair<Hand, Bid>> output;
  output.reserve(hand_bids.size());
  for (const auto& [hand, bid] : hand_bids) {
    output.emplace_back(ConvertJacksToJokers(hand), bid);
  }
  return output;
}

absl::StatusOr<std::pair<Hand, Bid>> ParseLine(const std::string& line) {
  if (line.size() < 7) {
    return absl::InvalidArgumentError("line too short");
  }
  // Parse the hand.
  auto hand = ParseHand(line.substr(0, 5));
  if (!hand.ok()) {
    return hand.status();
  }
  // Parse the bid.
  int b;
  if (!absl::SimpleAtoi(line.substr(6), &b)) {
    return absl::InvalidArgumentError("failed to parse bid");
  }
  return std::make_pair(*hand, b);
}

// Parse a set of lines.
absl::StatusOr<std::vector<std::pair<Hand, Bid>>> ParseLines(
    std::istream& input) {
  std::vector<std::pair<Hand, Bid>> hand_bids;
  std::string line;
  while (std::getline(input, line)) {
    auto hand_bid = ParseLine(line);
    if (!hand_bid.ok()) {
      return hand_bid.status();
    }
    hand_bids.push_back(*hand_bid);
  }
  return hand_bids;
}

absl::StatusOr<std::vector<std::pair<Hand, Bid>>> ParseLines(
    const std::string& input) {
  std::istringstream in_stream(input);
  return ParseLines(in_stream);
}

Type HandType(const Hand& hand) {
  std::unordered_map<Card, int> card_counts;
  for (Card c : hand) {
    ++card_counts[c];
  }
  int joker_count = card_counts.count(1) ? card_counts[1] : 0;
  int max_count = 0;
  for (const auto& [_, count] : card_counts) {
    max_count = std::max(max_count, count);
  }
  switch (card_counts.size()) {
    case 5:
      return joker_count ? Type::kPair : Type::kHighCard;
    case 4:
      return joker_count ? Type::kThreeOfAKind : Type::kPair;
    case 3:
      // 2 2 1 or 3 1 1.
      if (joker_count == 3) {
        return Type::kFourOfAKind;
      } else if (joker_count == 2) {
        return max_count == 3 ? Type::kFiveOfAKind : Type::kFourOfAKind;
      } else if (joker_count == 1) {
        return max_count == 3 ? Type::kFourOfAKind : Type::kFullHouse;
      } else {
        return max_count == 3 ? Type::kThreeOfAKind : Type::kTwoPair;
      }
    case 2:
      // 3 2 or 4 1.
      // Either four of a kind or a full house (without jokers).
      if (joker_count) {
        return Type::kFiveOfAKind;
      } else {
        return max_count == 4 ? Type::kFourOfAKind : Type::kFullHouse;
      }
    case 1:
      return Type::kFiveOfAKind;  // five of a kind
  }
  // Should really be an error code.
  return Type::kUnknownType;
}

// Sort a vector of hands and bids by their values.
std::vector<std::pair<Hand, Bid>> SortHandBids(
    const std::vector<std::pair<Hand, Bid>>& hand_bids) {
  std::vector<std::tuple<Type, Hand, Bid>> tuples;
  for (const auto& hand_bid : hand_bids) {
    tuples.push_back(
        {HandType(hand_bid.first), hand_bid.first, hand_bid.second});
  }
  // Now sort. (Arrays sort lexicographically, which is what we want.)
  std::sort(tuples.begin(), tuples.end());
  // Reconstruct the output.
  std::vector<std::pair<Hand, Bid>> output;
  for (const auto& [_, h, b] : tuples) {
    output.emplace_back(h, b);
  }
  return output;
}

// Get the total score, from the ranks.
int TotalWinnings(const std::vector<std::pair<Hand, Bid>>& hand_bids) {
  int score = 0;
  auto sorted = SortHandBids(hand_bids);
  for (int i = 0; i < sorted.size(); ++i) {
    score += (i + 1) * sorted[i].second;
  }
  return score;
}

}  // namespace aoc07
