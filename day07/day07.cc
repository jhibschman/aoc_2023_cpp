/*
Advent of code, day 7: https://adventofcode.com/2023/day/7
*/

#include <fstream>
#include <iostream>

#include "day07_lib.h"

namespace aoc07 {
namespace {
int DoPart1And2(std::istream& input) {
  auto hand_bids = ParseLines(input);
  if (!hand_bids.ok()) {
    std::cerr << hand_bids.status() << std::endl;
    return 1;
  }
  std::cout << TotalWinnings(*hand_bids) << std::endl;
  auto joker_hand_bids = ConvertJacksToJokers(*hand_bids);
  std::cout << TotalWinnings(joker_hand_bids) << std::endl;
}
}  // namespace
}  // namespace aoc07

int main(int argc, char* argv[]) {
  if (argc < 2) {
    std::cerr << "path required" << std::endl;
    return 1;
  }
  std::ifstream file;
  file.open(argv[1]);
  return aoc07::DoPart1And2(file);
}