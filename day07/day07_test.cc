#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <optional>
#include <sstream>
#include <string>

#include "day07_lib.h"

namespace aoc07 {
namespace {

constexpr char kTestData[] = R"(32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483
)";

TEST(HandTest, ParseLineEmpty) {
  auto hand_bid = ParseLine("");
  EXPECT_FALSE(hand_bid.ok());
}

TEST(HandTest, ParseLine) {
  auto hand_bid = ParseLine("32T3K 765");
  ASSERT_TRUE(hand_bid.ok());
  EXPECT_THAT(hand_bid->first, testing::ElementsAre(3, 2, 10, 3, 13));
  EXPECT_THAT(hand_bid->second, testing::Eq(765));
}

TEST(HandTest, ParseLines) {
  auto hand_bids = ParseLines(kTestData);
  ASSERT_TRUE(hand_bids.ok());
  EXPECT_THAT(*hand_bids, testing::ElementsAre(
                              testing::Pair(Hand{3, 2, 10, 3, 13}, 765),
                              testing::Pair(Hand{10, 5, 5, 11, 5}, 684),
                              testing::Pair(Hand{13, 13, 6, 7, 7}, 28),
                              testing::Pair(Hand{13, 10, 11, 11, 10}, 220),
                              testing::Pair(Hand{12, 12, 12, 11, 14}, 483)));
}

TEST(HandTest, HandType) {
  auto hand = ParseHand("T55J5");
  ASSERT_TRUE(hand.ok());
  EXPECT_EQ(HandType(*hand), Type::kThreeOfAKind);
}

TEST(HandTest, SortHandBids) {
  auto hand_bids = ParseLines(kTestData);
  ASSERT_TRUE(hand_bids.ok());
  auto sorted_hand_bids = SortHandBids(*hand_bids);
  EXPECT_THAT(
      sorted_hand_bids,
      testing::ElementsAre(testing::Pair(Hand{3, 2, 10, 3, 13}, 765),
                           testing::Pair(Hand{13, 10, 11, 11, 10}, 220),
                           testing::Pair(Hand{13, 13, 6, 7, 7}, 28),
                           testing::Pair(Hand{10, 5, 5, 11, 5}, 684),
                           testing::Pair(Hand{12, 12, 12, 11, 14}, 483)));
}

TEST(HandTest, TotalWinnings) {
  auto hand_bids = ParseLines(kTestData);
  ASSERT_TRUE(hand_bids.ok());
  EXPECT_EQ(TotalWinnings(*hand_bids), 6440);
}

TEST(HandTest, TotalWinningsWithJokers) {
  auto hand_bids = ParseLines(kTestData);
  ASSERT_TRUE(hand_bids.ok());
  auto joker_hand_bids = ConvertJacksToJokers(*hand_bids);
  EXPECT_EQ(TotalWinnings(joker_hand_bids), 5905);
}

}  // namespace
}  // namespace aoc07
