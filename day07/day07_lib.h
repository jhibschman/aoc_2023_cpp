#ifndef AOC_DAY_07_H_
#define AOC_DAY_07_H_

#include <array>
#include <iostream>
#include <string>
#include <utility>
#include <vector>

#include "absl/status/statusor.h"

namespace aoc07 {

// Card.
// enum class Card { kTwo = 2, ... }  // too much work
using Card = int;  // T=10 .. A=14; Jokers=1.

// Hand of cards.
using Hand = std::array<Card, 5>;

// Bid amount.
using Bid = int;

// Parse a hand of cards.
absl::StatusOr<Hand> ParseHand(const std::string& text);

// Convert any Jacks in the hand to Jokers.
Hand ConvertJacksToJokers(const Hand& hand);
std::vector<std::pair<Hand, Bid>> ConvertJacksToJokers(
    const std::vector<std::pair<Hand, Bid>>& hand_bids);

// Parse a line of input.
absl::StatusOr<std::pair<Hand, Bid>> ParseLine(const std::string& line);

// Parse a set of lines.
absl::StatusOr<std::vector<std::pair<Hand, Bid>>> ParseLines(
    std::istream& input);

absl::StatusOr<std::vector<std::pair<Hand, Bid>>> ParseLines(
    const std::string& input);

enum class Type {
  kUnknownType = 0,
  kHighCard,
  kPair,
  kTwoPair,
  kThreeOfAKind,
  kFullHouse,
  kFourOfAKind,
  kFiveOfAKind
};

// Calculate the rank of a hand, based on the type it is.
Type HandType(const Hand& hand);

// Sort a vector of hands and bids by their values.
std::vector<std::pair<Hand, Bid>> SortHandBids(
    const std::vector<std::pair<Hand, Bid>>& hand_bids);

// Get the total score, from the ranks.
int TotalWinnings(const std::vector<std::pair<Hand, Bid>>& hand_bids);

}  // namespace aoc07

#endif