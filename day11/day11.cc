/*
Advent of Code, day 11.
*/
#include <cmath>
#include <fstream>
#include <iostream>
#include <set>
#include <utility>
#include <vector>

#include "absl/status/status.h"
#include "absl/status/statusor.h"

namespace {
using Grid = std::vector<std::string>;
using Point = std::pair<int, int>;      // row, col

absl::StatusOr<Grid> ParseGrid(std::istream& input) {
  std::string line;
  Grid grid;
  int ncols = 0;
  while (std::getline(input, line)) {
    if (grid.empty()) {
      ncols = line.size();
    } else if (ncols != line.size()) {
      return absl::InvalidArgumentError("col size mismatch");
    }
    grid.push_back(line);
  }
  if (grid.empty()) {
    return absl::InvalidArgumentError("empty grid");
  }
  return grid;
}

int64_t TotalDistance(const Grid& grid, int expansion) {
  // Count values in rows and columns.
  int nrow = grid.size();
  int ncol = grid[0].size();
  // std::clog << "nrow: " << nrow << ", ncol: " << ncol << std::endl;
  std::vector<int> row_counts(nrow), col_counts(ncol);
  std::vector<Point> galaxies;
  for (int r = 0; r < nrow; ++r) {
    for (int c = 0; c < ncol; ++c) {
      if (grid[r][c] == '.') {
        continue;
      }
      galaxies.push_back({r, c});
      row_counts[r]++;
      col_counts[c]++;
    }
  }
  // Determine the row/col metric.
  std::vector<int> row_metric(nrow);
  int row_dist = 0;
  for (int i = 0; i < nrow; ++i) {
    row_metric[i] = row_dist;
    row_dist += row_counts[i] == 0 ? expansion : 1;
  }
  std::vector<int> col_metric(ncol);
  int col_dist = 0;
  for (int i = 0; i < ncol; ++i) {
    col_metric[i] = col_dist;
    col_dist += col_counts[i] == 0 ? expansion : 1;
  }
  // std::cout << "#galaxies=" << galaxies.size() << std::endl;
  // Sum the distances.
  int64_t tot_dist = 0;
  for (int i = 0; i < galaxies.size(); ++i) {
    auto [r1, c1] = galaxies[i];
    for (int j = i + 1; j < galaxies.size(); ++j) {
      auto [r2, c2] = galaxies[j];
      // std::clog << r1 << " " << c1 << " / " << r2 << " " << c2 << std::endl;
      tot_dist += abs(row_metric[r2] - row_metric[r1]);
      tot_dist += abs(col_metric[c2] - col_metric[c1]);
    }
  }
  return tot_dist;
}

absl::Status DoPart1And2(std::istream& input) {
  auto grid = ParseGrid(input);
  if (!grid.ok()) {
    return grid.status();
  }
  std::cout << TotalDistance(*grid, 2) << std::endl;
  std::cout << TotalDistance(*grid, 1'000'000) << std::endl;
  return absl::OkStatus();
}

}  // namespace

int main(int argc, char* argv[]) {
  if (argc < 2) {
    std::cerr << "path required" << std::endl;
    return 1;
  }
  std::ifstream input;
  input.open(argv[1]);
  auto status = DoPart1And2(input);
  if (!status.ok()) {
    std::cerr << status << std::endl;
    return 1;
  }
  return 0;
}
