# Advent of Code 2023 in C++

## About

This needs Bazel to build.

## To run

### Days 1 and 2

A typical invocation for day 1 or 2 is:

```
bazel run day01/day01 -- "$(realpath day01/data.txt)"
```

The `realpath` is needed to get an absolute path, since by default Bazel runs
from an internal build directory.

Alternately, you could run via:

```
bazel build day01/day01 && ./bazel-bin/day01/day01 day01/data.txt
```

### Days 3 and later

For day 3 and later, I switched to using a top-level `BUILD` file, so the invocation is:

```
bazel run day03 -- "$(realpath day03/data.txt)"
```

or

```
bazel build day03 && ./bazel-bin/day03 day03/data.txt
```
