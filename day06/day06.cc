/*
Advent of code, day 6.

Figure out the min/max times to beat a given distance.

Given a "boat" that you can "charge" in place to a given speed at 1 mm/ms^2, a
total available time in ms, and a distance to beat in mm, find the number of
charge times that would beat a previous record distance.

Version 1: there are 4 time/distance pairs, compute the product of the numbers
of times.

Version 2: there's just one (very large) time/distance pair.
*/

#include <cmath>
#include <cstdint>
#include <iostream>
#include <optional>
#include <utility>
#include <vector>

namespace {

// data
struct TimeDistance {
  int64_t time, distance;
};

// Version 1 data.
// Don't worry about copies, since it's small.
std::vector<TimeDistance> Data() {
  /*
  Time:        58     99     64     69
  Distance:   478   2232   1019   1071
  */
  return {
      {58, 478},
      {99, 2232},
      {64, 1019},
      {69, 1071},
  };
}

// Version 2 data.
std::vector<TimeDistance> Data2() {
  // Aggregate all the previous numbers together.
  return {
      {58'996'469, 478'223'210'191'071},
  };
}

// Minimum and maximum charge time to *beat* the distance in the available time.
std::optional<std::pair<int64_t, int64_t>> MinMaxCharge(TimeDistance td) {
  // Given the total time tt, if you charge for ct ms, you will travel
  // d = ct * (tt - ct). If this needs to be greater than the minimum
  // distance md, we have md < (tt - ct) * ct, so if we solve
  // (tt - ct) * ct = md, we'd be OK.
  //   ct^2 - tt * ct + md == 0.
  // So, ct = (1/2) (tt +/- sqrt(tt^2 - 4*md).
  //
  // In the big problem,
  //   tt is 6e7, so tt^2 is 4e15, so it fits in int64 (9e18).
  //   md is 5e14, so 4*md is 2e15, still fits in int64.
  // Double has 16 decimal places, so that can contain the full number.
  // No problem!
  int64_t discriminant = td.time * td.time - 4 * td.distance;
  if (discriminant <= 0) {
    // No solution.
    return std::nullopt;
  }
  double sqrt_d = sqrt(discriminant);
  double min_t = 0.5 * (td.time - sqrt_d);
  double max_t = 0.5 * (td.time + sqrt_d);
  int64_t imin_t = floor(min_t) + 1;
  int64_t imax_t = ceil(max_t) - 1;
  return std::make_pair(imin_t, imax_t);
}

int64_t SolutionProduct(const std::vector<TimeDistance>& tds) {
  int64_t prod = 1;
  for (TimeDistance td : tds) {
    auto min_max = MinMaxCharge(td);
    if (!min_max) {
      std::cerr << "no solution for t=" << td.time << ", d=" << td.distance
                << std::endl;
      return -1;
    }
    std::clog << "min/max for t=" << td.time << ", d=" << td.distance
              << " are min=" << min_max->first << ", max=" << min_max->second
              << std::endl;
    int n_sols = min_max->second - min_max->first + 1;
    prod *= n_sols;
  }
  return prod;
}

}  // namespace

int main(int argc, char* argv[]) {
  // Version 1.
  int64_t prod = SolutionProduct(Data());
  std::cout << prod << std::endl;
  // Version 2.
  prod = SolutionProduct(Data2());
  std::cout << prod << std::endl;
}
