/*
Advent of code, day 2: https://adventofcode.com/2023/day/2

Part 1: given a file of draws from a bag of colored cubes, like

  Game 4: 7 red, 2 green, 1 blue; 12 green; 12 green

Compute the sum of the game numbers where the cubes could have been
drawn from a bag containing 12 red cubes, 13 green cubes, and 14 blue cubes.

Part 2:

For each game, compute the minimum number of cubes needed for the
set of draws. Define the "power" as the product of these numbers.
Compute the sum of the powers of all of the games.
*/
#include <algorithm>
#include <fstream>
#include <iostream>
#include <optional>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

#include "absl/status/status.h"
#include "absl/status/statusor.h"
#include "absl/strings/numbers.h"
#include "absl/strings/str_split.h"
#include "absl/strings/string_view.h"

namespace {

// Counts of cubes drawn.
struct CubeCounts {
  int red, blue, green;
};

// Draws for a given game.
struct Game {
  int id;
  std::vector<CubeCounts> draws;
};

// Parse a line of text, return an error status on failure.
absl::StatusOr<Game> ParseLine(absl::string_view line) {
  size_t i = line.find(':');
  if (i == std::string::npos) {
    return absl::InvalidArgumentError("parse failed");
  }
  int game_id;
  // 5 = len("Game ")
  if (!absl::SimpleAtoi(line.substr(5, i - 5), &game_id)) {
    return absl::InvalidArgumentError("game id parse failed");
  }
  std::vector<absl::string_view> draws =
      absl::StrSplit(line.substr(i + 1), ';');
  Game game;
  game.id = game_id;
  for (auto draw : draws) {
    // draw is like " 14 green, 3 red"
    CubeCounts counts{};
    std::vector<absl::string_view> number_colors = absl::StrSplit(draw, ',');
    for (auto number_color : number_colors) {
      std::vector<absl::string_view> tokens =
          absl::StrSplit(number_color, ' ', absl::SkipEmpty());
      int count;
      if (!absl::SimpleAtoi(tokens[0], &count)) {
        return absl::InvalidArgumentError("count parse failed");
      }
      if (tokens[1] == "red") {
        counts.red = count;
      } else if (tokens[1] == "green") {
        counts.green = count;
      } else if (tokens[1] == "blue") {
        counts.blue = count;
      } else {
        return absl::InvalidArgumentError("unrecognized color");
      }
    }
    game.draws.push_back(counts);
  }
  return game;
}

// Used for debugging.
void PrintGame(const Game& game) {
  std::cout << "Game " << game.id << ": ";
  for (const auto& draw : game.draws) {
    if (draw.red > 0) {
      std::cout << draw.red << " red, ";
    }
    if (draw.green > 0) {
      std::cout << draw.green << " green, ";
    }
    if (draw.blue > 0) {
      std::cout << draw.blue << " blue, ";
    }
    std::cout << "; ";
  }
  std::cout << std::endl;
}

// The "power" is defined as the product of the minimum number of cubes
// required for the set of draws to be legal.
int GamePower(const Game& game) {
  CubeCounts mins{};
  for (const CubeCounts& counts : game.draws) {
    mins.blue = std::max(mins.blue, counts.blue);
    mins.green = std::max(mins.green, counts.green);
    mins.red = std::max(mins.red, counts.red);
  }
  return mins.blue * mins.green * mins.red;
}

// Sum a game; return the total counts.
absl::StatusOr<std::pair<int, int>> GameSum(std::istream& input,
                                            CubeCounts limits,
                                            bool debug = true) {
  std::string line;
  int id_sum = 0;
  int power_sum = 0;
  while (std::getline(input, line)) {
    auto game = ParseLine(line);
    if (!game.ok()) {
      return game.status();
    }
    if (debug) {
      std::cout << "line: " << line << std::endl;
      PrintGame(*game);
    }
    bool game_ok = true;
    for (const CubeCounts& counts : game->draws) {
      if (counts.red > limits.red || counts.green > limits.green ||
          counts.blue > limits.blue) {
        game_ok = false;
        break;
      }
    }
    if (game_ok) {
      id_sum += game->id;
    }
    power_sum += GamePower(*game);
  }
  return std::make_pair(id_sum, power_sum);
}

}  // namespace

int main(int argc, char* argv[]) {
  if (argc < 2) {
    std::cerr << "path needed" << std::endl;
    return 1;
  }
  const std::string path = argv[1];
  std::ifstream file;
  file.open(path);
  auto id_power =
      GameSum(file, {.red = 12, .green = 13, .blue = 14}, /*debug=*/false);
  if (id_power.ok()) {
    std::cout << "ID sum: " << id_power->first << std::endl;
    std::cout << "Power sum: " << id_power->second << std::endl;
    return 0;
  } else {
    std::cerr << "error: " << id_power.status() << std::endl;
    return 1;
  }
}