/*
Advent of Code, day 5: https://adventofcode.com/2023/day/5
*/

#include <fstream>
#include <iostream>

#include "day05_lib.h"

int main(int argc, char* argv[]) {
  if (argc < 2) {
    std::cerr << "path required" << std::endl;
    return 1;
  }
  std::ifstream file;
  file.open(argv[1]);
  return aoc05::DoPart1And2(file);
}
