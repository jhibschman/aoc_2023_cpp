#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <optional>
#include <sstream>
#include <string>

#include "day05_lib.h"

namespace aoc05 {
namespace {

constexpr char kTestAlmanac[] = R"(seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4
)";

TEST(RangeTest, EmptyLookup) {
  RangeMap rmap;
  EXPECT_EQ(rmap.Lookup(1), 1);
}

TEST(RangeTest, SingleLookup) {
  RangeMap rmap;
  rmap.AddRange(11, 1, 5);
  EXPECT_EQ(rmap.Lookup(1), 11);  // beginning
  EXPECT_EQ(rmap.Lookup(3), 13);  // mid
  EXPECT_EQ(rmap.Lookup(5), 15);  // end
  EXPECT_EQ(rmap.Lookup(6), 6);   // past
  EXPECT_EQ(rmap.Lookup(0), 0);   // before
}

TEST(RangeTest, MultiRange) {
  RangeMap rmap;
  rmap.AddRange(11, 1, 5);
  rmap.AddRange(21, 6, 5);
  EXPECT_EQ(rmap.Lookup(3), 13);  // first range
  EXPECT_EQ(rmap.Lookup(8), 23);  // second range
}

TEST(RangeTest, ParseEmpty) {
  auto rmap = RangeMap::Parse("");
  EXPECT_TRUE(rmap.ok());
}

TEST(RangeTest, ParseSingle) {
  auto rmap = RangeMap::Parse("50 98 2\n");
  ASSERT_TRUE(rmap.ok());
  EXPECT_EQ(rmap->Lookup(99), 51);
}

TEST(RangeTest, ParseDouble) {
  auto rmap = RangeMap::Parse(R"(50 98 2
52 50 48
)");
  ASSERT_TRUE(rmap.ok());
  EXPECT_EQ(rmap->Lookup(99), 51);
  EXPECT_EQ(rmap->Lookup(50), 52);
}

TEST(AlmanacTest, ParseSucceeds) {
  std::istringstream input(kTestAlmanac);
  auto almanac = Almanac::Parse(input);
  EXPECT_TRUE(almanac.ok());
}

TEST(AlmanacTest, ParseSeeds) {
  std::istringstream input(kTestAlmanac);
  auto almanac = Almanac::Parse(input);
  ASSERT_TRUE(almanac.ok());
  EXPECT_THAT(almanac->seeds(), testing::ElementsAre(79, 14, 55, 13));
}

TEST(AlmanacTest, ParseAndLookup) {
  std::istringstream input(kTestAlmanac);
  auto almanac = Almanac::Parse(input);
  ASSERT_TRUE(almanac.ok());
  EXPECT_EQ(almanac->LookupAll(79), 82);
  EXPECT_EQ(almanac->LookupAll(14), 43);
  EXPECT_EQ(almanac->LookupAll(55), 86);
  EXPECT_EQ(almanac->LookupAll(13), 35);
}

TEST(AlmanacTest, MinLocation) {
  std::istringstream input(kTestAlmanac);
  auto almanac = Almanac::Parse(input);
  ASSERT_TRUE(almanac.ok());
  EXPECT_EQ(MinLocation(*almanac), 35);
}

TEST(AlmanacTest, MinLocationRange) {
  std::istringstream input(kTestAlmanac);
  auto almanac = Almanac::Parse(input);
  ASSERT_TRUE(almanac.ok());
  EXPECT_EQ(MinLocationRange(*almanac), 46);
}

// TEST(Day05Test, NextLine) {
//   std::istringstream input(kTestAlmanac);
//   auto almanac = Almanac::Parse(input);
//   ASSERT_TRUE(almanac.ok());
//   std::string line;
//   std::getline(input, line);
//   EXPECT_EQ(line, "");
//   std::getline(input, line);
//   EXPECT_EQ(line, "x");
// }

// Demonstrate some basic assertions.
TEST(Day05Test, BasicAssertions) {
  // Expect two strings not to be equal.
  EXPECT_STRNE("hello", "world");
  // Expect equality.
  EXPECT_EQ(7 * 6, 42);
}

}  // namespace
}  // namespace aoc05
