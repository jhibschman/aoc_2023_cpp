// Library code for day 5.
#ifndef AOC_DAY_05_H_
#define AOC_DAY_05_H_

#include <cstdint>
#include <iostream>
#include <optional>
#include <vector>

#include "absl/status/statusor.h"

namespace aoc05 {

// Map a set of ranges from source ints to destinations.
class RangeMap {
 public:
  // Parse a RangeMap from a stream.
  static absl::StatusOr<RangeMap> Parse(std::istream& input);
  // Parse a RangeMap from a string.
  static absl::StatusOr<RangeMap> Parse(const std::string& input);
  // Add a source->destination range to the map.
  void AddRange(int64_t destination, int64_t source, int64_t size);
  // Look up a source value in the map.
  int64_t Lookup(int64_t source) const;

 private:
  struct Range {
    int64_t destination, source, size;
  };
  std::vector<Range> ranges_;
};

// A list of seeds to plant, and various ranges of soil types.
class Almanac {
 public:
  Almanac() {}
  Almanac(const std::vector<int64_t> seeds) : seeds_(seeds) {}

  // Parse the almanac information.
  static absl::StatusOr<Almanac> Parse(std::istream& input);

  // Add a map to the list of consecutive maps.
  void AddMap(const RangeMap& rmap);

  // Look up a value in all maps.
  int64_t LookupAll(int64_t seed) const;

  // Accessors.
  const std::vector<int64_t> seeds() const { return seeds_; }

 private:
  std::vector<int64_t> seeds_;
  std::vector<RangeMap> rmaps_;
};

// Return the minimum location for any seed.
int64_t MinLocation(const Almanac& almanac);

// Return the minimum location, interpreting seeds as ranges.
int64_t MinLocationRange(const Almanac& almanac);

// Do part 1, return 0 on success, 1 on failure.
int DoPart1(std::istream& input);

// Do part 1 and 2, return 0 on success, 1 on failure.
int DoPart1And2(std::istream& input);

}  // namespace aoc05

#endif  // AOC_DAY_05_H_
