// Library code for day 5.
#include "day05_lib.h"

#include <algorithm>
#include <iostream>
#include <iterator>
#include <string>
#include <vector>

#include "absl/status/statusor.h"

namespace aoc05 {

// static
absl::StatusOr<RangeMap> RangeMap::Parse(std::istream& input) {
  // Expected format is a list of int triples. A blank line terminates (and is
  // consumed).
  RangeMap rmap;
  std::string line;
  while (std::getline(input, line)) {
    if (line.empty()) {
      break;
    }
    // TODO: There's got to be a better way to parse this.
    int64_t destination, source, size;
    std::istringstream line_stream(line);
    line_stream >> destination >> source >> size;
    rmap.AddRange(destination, source, size);
  }
  // TODO: Error checking.
  return rmap;
}

// static
absl::StatusOr<RangeMap> RangeMap::Parse(const std::string& input) {
  std::istringstream input_stream(input);
  return Parse(input_stream);
}

void RangeMap::AddRange(int64_t destination, int64_t source, int64_t size) {
  ranges_.push_back({destination, source, size});
}

int64_t RangeMap::Lookup(int64_t source) const {
  // Simple linear lookup, since these aren't very big.
  for (const auto& range : ranges_) {
    if (range.source <= source && source < range.source + range.size) {
      return range.destination + (source - range.source);
    }
  }
  return source;
}

// static
absl::StatusOr<Almanac> Almanac::Parse(std::istream& input) {
  // First line: "seeds: 79 14 55 13"
  std::string line;
  std::getline(input, line);

  std::istringstream seed_str(line.substr(7));  // skip "seeds: "
  std::istream_iterator<int64_t> seeds_begin(seed_str), seeds_end;
  std::vector<int64_t> seeds(seeds_begin, seeds_end);

  std::getline(input, line);  // skip blank line

  Almanac almanac(seeds);

  // Now parse each map in turn.
  for (int i = 0; i < 7; ++i) {  // 7 maps
    std::getline(input, line);   // skip name line
    // std::clog << "skip: " << line << std::endl;
    auto rmap = RangeMap::Parse(input);
    if (!rmap.ok()) {
      return rmap.status();
    }
    almanac.AddMap(*rmap);
  }
  return almanac;
}

void Almanac::AddMap(const RangeMap& rmap) { rmaps_.push_back(rmap); }

int64_t Almanac::LookupAll(int64_t seed) const {
  int64_t curr = seed;
  // std::clog << "init: " << curr << ", #maps: " << rmaps_.size() << std::endl;
  for (const auto& rmap : rmaps_) {
    curr = rmap.Lookup(curr);
    // std::clog << "next: " << curr << std::endl;
  }
  return curr;
}

int64_t MinLocation(const Almanac& almanac) {
  bool first = true;
  int64_t min_loc;
  for (int64_t seed : almanac.seeds()) {
    if (first) {
      min_loc = almanac.LookupAll(seed);
      first = false;
    } else {
      min_loc = std::min(min_loc, almanac.LookupAll(seed));
    }
  }
  return min_loc;
}

// Instead of individual seeds, we interpret the seeds() list as
// a set of (start seed, size of range) pairs, i.e. [start, start.size).
int64_t MinLocationRange(const Almanac& almanac) {
  if (almanac.seeds().empty() || almanac.seeds().size() % 2 != 0) {
    return -1;
  }
  // Brute force, but that's OK for now. A smarter method would be to compute
  // the composition of the RangeMaps, and keep track (sorted) of the starts of
  // these ranges, so I could answer questions about entire ranges at once.
  int64_t min_loc = almanac.LookupAll(almanac.seeds()[0]);
  for (int i = 0; i < almanac.seeds().size(); i += 2) {
    int64_t start = almanac.seeds()[i];
    int64_t size = almanac.seeds()[i + 1];
    for (int64_t seed = start; seed < start + size; ++seed) {
      min_loc = std::min(min_loc, almanac.LookupAll(seed));
    }
  }
  return min_loc;
}

int DoPart1(std::istream& input) {
  auto almanac = Almanac::Parse(input);
  if (!almanac.ok()) {
    return 1;
  }
  int64_t min_loc = MinLocation(*almanac);
  std::cout << min_loc << std::endl;
  return 0;
}

int DoPart1And2(std::istream& input) {
  auto almanac = Almanac::Parse(input);
  if (!almanac.ok()) {
    return 1;
  }
  int64_t min_loc = MinLocation(*almanac);
  std::cout << min_loc << std::endl;
  min_loc = MinLocationRange(*almanac);
  std::cout << min_loc << std::endl;
  return 0;
}

}  // namespace aoc05
