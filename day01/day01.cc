/*
https://adventofcode.com/2023/day/1

For each line of input, find the first digit and the last digit in the line.
Combine those into a two-digit number.
Sum those numbers and report the result.

In version 1, we just accept literal digits.
In version 2, we also accept number-words, like "one", "two", etc.
(But not zero, for some reason.)
*/

#include <cstring>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

namespace {

constexpr char kDigits[] = "0123456789";

// Returns the first digit in the line.
int FirstDigit(const std::string& line) {
  size_t i = line.find_first_of(kDigits);
  if (i == std::string::npos) {
    return -1;
  }
  return line[i] - '0';
}

// Returns the last digit in the line.
int LastDigit(const std::string& line) {
  size_t i = line.find_last_of(kDigits);
  if (i == std::string::npos) {
    return -1;
  }
  return line[i] - '0';
}

// Use static initialization to avoid race conditions.
// Static value is just a pointer, so destruction has no problem.
const std::vector<const std::string>* DigitWords() {
  static std::vector<const std::string>* digit_words = []() {
    return new std::vector<const std::string>{
        "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
    };
  }();
  return digit_words;
}

// Returns -1 if no digit found.
int FindDigitOrWord(const std::string& line, bool last) {
  size_t best_pos = -1;
  int best_val = 0;
  // First, try to find a digit.
  size_t i = last ? line.find_last_of(kDigits) : line.find_first_of(kDigits);
  if (i != std::string::npos) {
    best_pos = i;
    best_val = line[i] - '0';
  }
  // Then, look for each digit word in turn.
  int digit_val = 0;  // We know digits are in order, starting with 1.
  for (const std::string& digit : *DigitWords()) {
    ++digit_val;
    i = last ? line.rfind(digit) : line.find(digit);
    if (i == std::string::npos) {
      continue;
    }
    if (best_pos == -1 || (last && i > best_pos) || (!last && i < best_pos)) {
      best_pos = i;
      best_val = digit_val;
    }
  }
  return best_pos == -1 ? -1 : best_val;
}

// Version 1 of the line value, -1 on error.
int LineValue1(const std::string& line) {
  int d1 = FirstDigit(line);
  int d2 = LastDigit(line);
  if (d1 == -1 || d2 == -1) {
    return -1;
  }
  return d1 * 10 + d2;
}

// Version 2 of line value.
int LineValue2(const std::string& line) {
  int d1 = FindDigitOrWord(line, /*last=*/false);
  int d2 = FindDigitOrWord(line, /*last=*/true);
  if (d1 == -1 || d2 == -1) {
    return -1;
  }
  return d1 * 10 + d2;
}

}  // namespace

int main(int argc, char* argv[]) {
  if (argc < 2) {
    std::cerr << "path required" << std::endl;
    return 1;
  }
  // Optionally invoke v1 via a "1" argument.
  // TODO(johannh): Check errors.
  int version = 2;
  if (argc > 2 && strcmp(argv[2], "1") == 0) {
    version = 1;
  }
  std::clog << "Reading " << argv[1] << std::endl;
  std::ifstream instream;
  instream.open(argv[1]);
  std::string line;
  int sum = 0;
  while (std::getline(instream, line)) {
    int val = -1;
    switch (version) {
      case 1:
        val = LineValue1(line);
        break;
      case 2:
        val = LineValue2(line);
        break;
      default:
        std::cerr << "unrecognized version: " << val << std::endl;
        return 1;
    }
    if (val == -1) {
      std::cerr << "error: number not found in line: " << line << std::endl;
      return 1;
    }
    sum += val;
  }
  // Output the sum.
  std::cout << sum << std::endl;
  return 0;
}
