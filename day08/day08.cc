/*
Advent of code, day 8.

Graph traversal.
*/

#include <fstream>
#include <iostream>
#include <map>
#include <numeric>
#include <set>
#include <sstream>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <utility>

#include "absl/status/status.h"
#include "absl/status/statusor.h"

namespace {
using Node = std::string;
using LeftRight = std::pair<Node, Node>;
using Graph = std::unordered_map<Node, LeftRight>;

// Intern the graph.
class InternedGraph {
public:
  using INode = int;
  using ILeftRight = std::pair<INode, INode>;

  InternedGraph(const Graph& in_graph) {
    // Initialize all the nodes.
    for (const auto& [n, lr] : in_graph) {
      node_to_inode_[n] = size_++;
      inode_to_node_.push_back(n);
      is_start_.push_back(n[2] == 'A');
      is_end_.push_back(n[2] == 'Z');
    }
    // Create the linkages.
    graph_.resize(size_);
    for (const auto& [n, lr] : in_graph) {
      int inode = node_to_inode_[n];
      int ileft = node_to_inode_[lr.first];
      int iright = node_to_inode_[lr.second];
      graph_[inode] = std::make_pair(ileft, iright);
    }
  };

  std::vector<INode> StartNodes() const {
    std::vector<INode> nodes;
    for (int i = 0; i < size_; ++i) {
      if (is_start_[i]) {
        nodes.push_back(i);
      }
    }
    return nodes;
  }

  void Advance(std::vector<INode>& nodes, char dir) const {
    if (dir == 'L') {
      for (INode& node : nodes) {
        node = graph_[node].first;
      }
    } else {
      for (INode& node : nodes) {
        node = graph_[node].second;
      }
    }
  }

  bool AllAtEnd(std::vector<INode>& nodes) {
    for (INode n : nodes) {
      if (!is_end_[n]) {
        return false;
      }
    }
    return true;
  }

  bool IsEnd(INode node) const {
    return is_end_[node];
  }

  struct Cycle {
    INode start_node;
    int64_t lead_in;
    int64_t cycle_length;
    std::set<int64_t> ends_in_lead_in;
    std::set<int64_t> ends_in_cycle;
  };

  static void PrintCycle(const Cycle& c) {
    std::clog << "Cycle{start=" << c.start_node << ", lead=" << c.lead_in
              << ", cycle_length=" << c.cycle_length
              << ", #lead_in_ends=" << c.ends_in_lead_in.size()
              << ", cycle_ends=[";
    for (const auto offset : c.ends_in_cycle) {
      std::clog << offset << ",";
    }
    std::clog << "]}" << std::endl;
  }

  // Finds the index of the first repeat node, and the length of the cycle.
  // It's not this simple, since we need to know which points along the cycle
  // are "end points". (We can assume that the lead-in doesn't matter.)
  // No, this isn't helpful, because of dirs. We need to be in the same phase
  // of both the dirs and the cycle.
  Cycle FindCycleFrom(INode start_node, const std::string& dirs) {
    using Visited = std::pair<INode, int64_t>; // node, dir_pos
    std::map<Visited, int64_t> first_seen;
    std::set<Visited> end_nodes;
    int64_t moves = 0;
    Visited cur{start_node, moves};
    while (!first_seen.count(cur)) {
      first_seen[cur] = moves;
      if (IsEnd(cur.first)) {
        end_nodes.insert(cur);
      }
      ++moves;
      INode next_node = dirs[cur.second] == 'L' ? graph_[cur.first].first : graph_[cur.first].second;
      int64_t dir_pos = moves % dirs.size();
      cur = {next_node, dir_pos};
    }
    // We returned to the same place.
    int64_t lead_in = first_seen[cur];
    int64_t cycle_length = moves - lead_in;
    std::set<int64_t> ends_in_lead_in;
    std::set<int64_t> ends_in_cycle;
    for (const auto& v : end_nodes) {
      int64_t i = first_seen[v];
      if (i < lead_in) {
        ends_in_lead_in.insert(i);
      } else {
        ends_in_cycle.insert(i - lead_in);
      }
    }
    return {start_node, lead_in, cycle_length, ends_in_lead_in, ends_in_cycle};
  }

private:
  int size_ = 0;
  std::unordered_map<Node, INode> node_to_inode_;
  std::vector<Node> inode_to_node_;
  std::vector<ILeftRight> graph_;
  std::vector<bool> is_start_;
  std::vector<bool> is_end_;
};

// Parse the input file.
absl::StatusOr<std::pair<std::string, Graph>> ParseFile(std::istream& input) {
  std::string dirs;
  std::getline(input, dirs);  // line of Ls and Rs.
  std::string line;
  std::getline(input, line);  // empty line
  Graph graph;
  while (std::getline(input, line)) {
    // line like: RGT = (HDG, QJV)
    std::string node = line.substr(0, 3);
    std::string left = line.substr(7, 3);
    std::string right = line.substr(12, 3);
    // std::clog << node << " -> (" << left << ", " << right << ")" << std::endl;
    graph[node] = {left, right};
  }
  return std::make_pair(std::move(dirs), std::move(graph));
}

bool AllAtDestination(const std::vector<Node>& nodes) {
  for (const Node& node : nodes) {
    if (node[2] != 'Z') {
      return false;
    }
  }
  return true;
}

void Advance(std::vector<Node>& nodes, const Graph& graph, char dir) {
  bool left = dir == 'L';
  auto end = graph.end();
  for (Node& node : nodes) {
    auto it = graph.find(node);
    if (it == end) {
      std::cerr << "lookup failed" << std::endl;
      return;
    }
    node = left ? it->second.first : it->second.second;
  }
}

std::istringstream TestInput2() {
  return std::istringstream(R"(LR

11A = (11B, XXX)
11B = (XXX, 11Z)
11Z = (11B, XXX)
22A = (22B, XXX)
22B = (22C, 22C)
22C = (22Z, 22Z)
22Z = (22B, 22B)
XXX = (XXX, XXX)
)");
}

int DoPart1(const std::string& dirs, const Graph& graph) {
  int n = dirs.size();
  int moves = 0;
  std::clog << "n=" << n << ", graph.size()=" << graph.size() << std::endl;
  // Part 1 - go from AAA to ZZZ.
  Node cur("AAA"), dest("ZZZ");
  while (cur != dest) {
    auto it = graph.find(cur);
    if (it == graph.end()) {
      std::cerr << "lookup failed" << std::endl;
      return -1;
    }
    cur = dirs[moves % n] == 'L' ? it->second.first : it->second.second;
    if (cur.empty()) {
      std::cerr << "lookup failed" << std::endl;
      return 1;
    }
    ++moves;
    if (moves % 1'000'000 == 0) {
      std::clog << "moves=" << moves << std::endl;
    }
  }
  return moves;
}

int64_t DoPart2(const std::string& dirs, const Graph& graph) {
  size_t n = dirs.size();
  std::vector<Node> curs;
  for (const auto& [k, _] : graph) {
    if (k[2] == 'A') {
      // std::cout << k << std::endl;
      curs.push_back(k);
    }
  }
  std::clog << "curs.size()=" << curs.size() << ", curs[0]=" << curs[0]
            << std::endl;
  int64_t moves2 = 0;
  while (!AllAtDestination(curs)) {
    if (false) {
      for (const Node& node : curs) {
        std::clog << node << ", ";
      }
      std::clog << std::endl;
    }
    Advance(curs, graph, dirs[moves2 % n]);
    ++moves2;
    if (moves2 % 10'000'000 == 0) {
      std::clog << "moves2=" << moves2;
      for (const Node& node : curs) {
        std::clog << " " << (node[2] == 'Z');
      }
      std::clog << std::endl;
    }
  }
  return moves2;
}

int64_t DoPart2v2(const std::string& dirs, const Graph& graph) {
  int64_t moves = 0;
  size_t n = dirs.size();
  InternedGraph igraph(graph);
  std::vector<InternedGraph::INode> curs = igraph.StartNodes();
  std::clog << "curs.size()=" << curs.size() << ", curs[0]=" << curs[0]
            << std::endl;
  while (!igraph.AllAtEnd(curs)) {
    igraph.Advance(curs, dirs[moves % n]);
    ++moves;
    if (moves % 10'000'000 == 0) {
      std::clog << "moves=" << moves;
      for (const InternedGraph::INode& node : curs) {
        std::clog << " " << igraph.IsEnd(node);
      }
      std::clog << std::endl;
    }
  }
  return moves;
}

int64_t DoPart2v3(const std::string& dirs, const Graph& graph) {
  InternedGraph igraph(graph);
  std::vector<InternedGraph::INode> curs = igraph.StartNodes();
  std::clog << "curs.size()=" << curs.size() << ", curs[0]=" << curs[0]
            << std::endl;
  int64_t cycle_lcm = 1;
  // This isn't the general solution (doesn't correctly handle offsets), but it
  // works for this particular case.
  for (int node : curs) {
    InternedGraph::Cycle c = igraph.FindCycleFrom(node, dirs);
    InternedGraph::PrintCycle(c);
    cycle_lcm = std::lcm(cycle_lcm, c.cycle_length);
  }
  return cycle_lcm;
}

}  // namespace

int main(int argc, char* argv[]) {
  if (argc < 2) {
    std::cerr << "requires path" << std::endl;
    return 1;
  }
  bool do_part_1 = false;
  bool do_part_2 = true;

  std::ifstream input;
  input.open(argv[1]);
  // std::istringstream input = TestInput2();
  
  auto dirs_graph = ParseFile(input);
  if (!dirs_graph.ok()) {
    std::cerr << "parsed failed" << std::endl;
    std::cerr << dirs_graph.status() << std::endl;
    return 1;
  }
  auto [dirs, graph] = *dirs_graph;

  if (do_part_1) {
    std::cout << DoPart1(dirs, graph) << std::endl;
  }

  // Part 2 - go from all nodes ending in A to all nodes ending in Z.
  if (do_part_2) {
    std::cout << DoPart2v3(dirs, graph) << std::endl;
  }
  return 0;
}
