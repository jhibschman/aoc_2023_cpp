/*
Advent of code, day 4: https://adventofcode.com/2023/day/4

Sample data is:

  Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
  Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
  Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
  Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
  Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
  Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11

A "match" is each number on the right side that matches one of the numbers on
the left side.

Part 1: find the total "points", where the number of points for each card is the
0 if no matches, otherwise 2^(matches-1).

Part 2: N matches on card i instead gives you an extra copy of cards i+1 through
i+N. How many cards do you have at the end, including the ones you started with?
*/

#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>
#include <numeric>
#include <set>
#include <sstream>
#include <string>
#include <utility>
#include <vector>

#include "absl/status/status.h"
#include "absl/status/statusor.h"
#include "absl/strings/numbers.h"
#include "absl/strings/string_view.h"

namespace {

struct Card {
  std::vector<int> win_numbers;
  std::vector<int> card_numbers;
};

// Parse a card from a string.
absl::StatusOr<Card> ParseCard(absl::string_view line) {
  int colon = line.find(':');
  int bar = line.find('|');
  if (colon == std::string::npos || bar == std::string::npos) {
    return absl::InvalidArgumentError("separator not found");
  }

  std::istringstream win_section(
      std::string(line.substr(colon + 1, bar - colon)));
  std::istream_iterator<int> win_begin(win_section), end;
  std::vector<int> win_numbers(win_begin, end);
  // TODO: check error codes on iterator/stream.

  std::istringstream card_section(std::string(line.substr(bar + 1)));
  std::istream_iterator<int> card_begin(card_section);
  std::vector<int> card_numbers(card_begin, end);
  // TODO: check error codes on iterator/stream.

  return Card{.win_numbers = std::move(win_numbers),
              .card_numbers = std::move(card_numbers)};
}

// Count the matching numbers on a card.
int CardMatches(const Card& card) {
  int matches = 0;
  std::set<int> wins(card.win_numbers.begin(), card.win_numbers.end());
  for (int n : card.card_numbers) {
    matches += wins.count(n);
  }
  return matches;
}

// Output a card to the log output, with newline.
void PrintCard(const Card& card) {
  std::clog << "Card(wins=[";
  for (int n : card.win_numbers) {
    std::clog << n << ",";
  }
  std::clog << "]; card=[";
  for (int n : card.card_numbers) {
    std::clog << n << ",";
  }
  std::clog << "])" << std::endl;
}

// Read the cards from input, return a vector of the matches on each card.
absl::StatusOr<std::vector<int>> ReadAndCollectMatches(std::istream& input,
                                                       bool debug = false) {
  std::vector<int> matches;
  std::string line;
  while (std::getline(input, line)) {
    auto card = ParseCard(line);
    if (!card.ok()) {
      return card.status();
    }
    if (debug) {
      PrintCard(*card);
    }
    matches.push_back(CardMatches(*card));
  }
  return matches;
}

// Total score in points for a card. 1 match is worth 1 point, doubling for each
// additional match.
int TotalScore(const std::vector<int>& matches) {
  int score = 0;
  for (int m : matches) {
    if (m) {
      score += 1 << (m - 1);
    }
  }
  return score;
}

// Total number of cards won, where getting N matches on a card means
// you win an extra copy of the the next N cards.
int TotalCards(const std::vector<int>& matches) {
  // We start with one of each card.
  std::vector<int> counts(matches.size(), 1);
  for (int i = 0; i < matches.size(); ++i) {
    int jmax = std::min(i + 1 + matches[i], static_cast<int>(matches.size()));
    for (int j = i + 1; j < jmax; ++j) {
      counts[j] += counts[i];
    }
  }
  return std::accumulate(counts.begin(), counts.end(), 0);
}

// Read the cards from the input and output the final score.
absl::Status DoPart1(std::istream& input, bool debug = false) {
  auto matches = ReadAndCollectMatches(input, debug);
  if (!matches.ok()) {
    return matches.status();
  }
  std::cout << TotalScore(*matches) << std::endl;
  return absl::OkStatus();
}

absl::Status DoPart1And2(std::istream& input, bool debug = false) {
  auto matches = ReadAndCollectMatches(input, debug);
  if (!matches.ok()) {
    return matches.status();
  }
  // Part 1
  std::cout << TotalScore(*matches) << std::endl;
  // Part 2
  std::cout << TotalCards(*matches) << std::endl;
  return absl::OkStatus();
}

}  // namespace

int main(int argc, char* argv[]) {
  if (argc < 2) {
    std::cerr << "path requried" << std::endl;
    return 1;
  }
  absl::string_view path(argv[1]);
  std::ifstream file;
  file.open(path);
  absl::Status status = DoPart1And2(file);
  if (!status.ok()) {
    std::cerr << "error: " << status << std::endl;
    return 1;
  }
  return 0;
}
